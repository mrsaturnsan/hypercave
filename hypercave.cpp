#include "includes.hpp"
#include "macros.hpp"
#include "engine.hpp"
#include "behaviors.hpp"
#include "game.hpp"

int main()
{
    try {
        HyperCave::Game().Execute();
    } catch (const std::exception& e) {
        std::cerr << e.what() << '\n';
        return HyperCave::ExitCode::ERROR;
    } catch (...) {
        std::cerr << "Unknown exception was thrown\n";
        return HyperCave::ExitCode::ERROR;
    }

    return HyperCave::ExitCode::SUCCESS;
}
