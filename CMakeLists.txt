cmake_minimum_required(VERSION 3.10 FATAL_ERROR)

find_program(CCACHE_PROGRAM ccache)
if (CCACHE_PROGRAM)
    set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE "${CCACHE_PROGRAM}")
endif()

project(Hyper_Cave VERSION 0.1.0)

# List source files being used
set(SourceFiles
    "${CMAKE_SOURCE_DIR}/hypercave.cpp"
)

add_executable(Hyper_Cave ${SourceFiles})

set(DEBUG_OPTIONS 
    -Og 
    -D_DEBUG
    -g
)

set(RELEASE_OPTIONS 
    -g
    -03 
    -DNDEBUG 
    -D_RELEASE
    -march=native
)

target_compile_options(Hyper_Cave PUBLIC "$<$<CONFIG:DEBUG>:${DEBUG_OPTIONS}>")
target_compile_options(Hyper_Cave PUBLIC "$<$<CONFIG:RELEASE>:${RELEASE_OPTIONS}>")

target_compile_options(Hyper_Cave
    PRIVATE
        -Wall 
        -Wextra 
        -Wpedantic 
        #-Werror
)

SET(CMAKE_SKIP_BUILD_RPATH FALSE)

target_compile_features(Hyper_Cave
   PRIVATE
       cxx_constexpr
       cxx_return_type_deduction
       cxx_lambda_init_captures
)

set_target_properties(Hyper_Cave
    PROPERTIES
        CXX_STANDARD 17
        CXX_STANDARD_REQUIRED ON
        CXX_EXTENSIONS OFF
)

find_package(glm REQUIRED)
find_package(OpenGL REQUIRED)
find_package(GLUT REQUIRED)
find_package(GLEW REQUIRED)
find_package(glfw3 REQUIRED)

if (APPLE)
    set(IRRKLANG
        "${CMAKE_SOURCE_DIR}/IrrKlang/bin/macosx-gcc/libirrklang.dylib"
    )
elseif (WIN32)
    set(IRRKLANG
        "${CMAKE_SOURCE_DIR}/IrrKlang/lib/Winx64-visualStudio/irrKlang.lib"
    )
endif ()

# Set the include directory
target_include_directories(Hyper_Cave 
    PRIVATE 
        ${CMAKE_SOURCE_DIR}
        ${CMAKE_SOURCE_DIR}/includes/
)

target_link_libraries(Hyper_Cave
    PRIVATE
        ${OPENGL_gl_LIBRARY}
        ${GLUT_LIBRARIES}
        ${GLEW_LIBRARIES}
        glfw
        ${IRRKLANG}
        glm
)

file(COPY ${CMAKE_SOURCE_DIR}/Resources DESTINATION ${CMAKE_CURRENT_BINARY_DIR})

# Copy dlls
if (APPLE)
    add_custom_command(TARGET Hyper_Cave PRE_BUILD
                       COMMAND ${CMAKE_COMMAND} -E copy
                       ${CMAKE_SOURCE_DIR}/IrrKlang/bin/macosx-gcc/libirrklang.dylib 
                       ${CMAKE_CURRENT_BINARY_DIR}/Resources/dlls/libirrklang.dylib)

    add_custom_command(TARGET Hyper_Cave PRE_BUILD
                       COMMAND ${CMAKE_COMMAND} -E copy
                       ${CMAKE_SOURCE_DIR}/IrrKlang/bin/macosx-gcc/ikpMP3.dylib
                       ${CMAKE_CURRENT_BINARY_DIR}/ikpMP3.dylib)
elseif (WIN32)
    add_custom_command(TARGET Hyper_Cave PRE_BUILD
                       COMMAND ${CMAKE_COMMAND} -E copy
                       ${CMAKE_SOURCE_DIR}/IrrKlang/bin/winx64-visualStudio/ikpMP3.dll
                       ${CMAKE_CURRENT_BINARY_DIR}/Resources/dlls/ikpMP3.dll)
endif ()
