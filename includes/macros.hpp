#ifndef MACROS_HPP
#define MACROS_HPP

#define UNUSED(x) static_cast<void>(x)
#define TOSTRING(x) #x
#define PASTE(x, y) x##y

#endif
