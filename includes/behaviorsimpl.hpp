#ifndef BEHAVIORS_IMPL_HPP
#define BEHAVIORS_IMPL_HPP

namespace HyperCave {
    inline Timer::Timer(float duration, std::function<void(Actor&)> callback) noexcept : m_duration{duration}, m_callback{callback}
    {

    }

    inline void Timer::DoAction(Actor& actor, float dt)
    {
        m_duration -= dt;
        if (m_duration <= 0) {
            m_callback(actor);
            actor.RemoveComponent<Timer>();
        }
    }

    inline BulletBehavior::BulletBehavior(const glm::vec2& move_direction, float move_speed) noexcept : m_move_direction{move_direction}, m_move_speed{move_speed}
    {

    }

    inline void BulletBehavior::DoAction(class Actor& actor, float dt)
    {
        actor.m_transform.Translate(m_move_direction * m_move_speed * dt);
        actor.m_collider.SetCenter(actor.m_transform.GetPosition());
        auto bounds = Game::GetSingleton().m_camera->GetWorldBounds();
        auto bound_test_r = actor.m_collider.IsInBounding(bounds.first, bounds.second);
        if (!bound_test_r.first) {
            m_move_direction = CircleCollider::GetReflection(m_move_direction, bound_test_r.second);
        }
    }

    inline PlayerBehavior::PlayerBehavior() : m_velocity{0}, m_shoot{false}
    {
        Game::GetSingleton().m_imanager.m_mouse_button_callbacks["PlayerClick"] = [this](int button, int action, int mods) mutable {
            if (button == GLFW_MOUSE_BUTTON_1) {
                if (action == GLFW_PRESS) {
                    m_shoot = true;
                } else if (action == GLFW_RELEASE) {
                    m_shoot = false;
                } 
            }
        };
    }

    inline PlayerBehavior::~PlayerBehavior() noexcept
    {
        Game::GetSingleton().m_imanager.m_mouse_button_callbacks.erase("PlayerClick");
    }

    inline void PlayerBehavior::DoAction(Actor& actor, float dt)
    {
        // if click, shoot bullet
        if (m_shoot) {
            Shoot(actor.m_transform.GetPosition());
        }

        // apply drag to bullets
        m_velocity -= (DRAG * m_velocity * dt);
        actor.m_transform.Translate(m_velocity * dt);  
        actor.m_collider.SetCenter(actor.m_transform.GetPosition());      

        // check if in world
        auto bounds = Game::GetSingleton().m_camera->GetWorldBounds();
        auto bound_test_r = actor.m_collider.IsInBounding(bounds.first, bounds.second);
        if (!bound_test_r.first) {
            m_velocity = CircleCollider::GetReflection(m_velocity, bound_test_r.second);
        }
    }

    inline void PlayerBehavior::Shoot(const glm::vec2& actorpos)
    {
        const glm::vec2 direction = glm::normalize(Game::GetSingleton().m_mouse_pos - actorpos);

        // create a bullet that moves towards direction
        {
            std::shared_ptr<Actor> bullet = std::make_shared<Actor>("Bullet");
            bullet->m_material = std::make_unique<SpriteMaterial>(*Game::GetSingleton().m_shaders["sprite_shader"], *Game::GetSingleton().m_textures["circle"]);
            bullet->m_transform.Scale(glm::vec2{Game::GetSingleton().m_textures["circle"]->GetAspect() * 1.0f, 1.0f});
            bullet->m_transform.SetPosition(actorpos + direction * 5.0f);
            bullet->m_transform.SetZPosition(0.2f);
            static std::random_device rd;
            static std::mt19937 gen(rd());
            static std::uniform_real_distribution<> dis(0, 1.0f);
            bullet->m_material->GetMaterial<SpriteMaterial>().m_color = glm::vec4(dis(gen), dis(gen), dis(gen), 1.0f);
            bullet->m_collider.SetRadius(0.5f);
            bullet->CreateBehavior<BulletBehavior>(direction, BULLET_VELOCITY);
            //bullet->CreateBehavior<Timer>(3.0f, [](Actor& a) { a.m_destroy = true; });
            Game::GetSingleton().m_actor_pool.AddActor(std::move(bullet));
        }

        //m_velocity -= direction * KNOCK_BACK;
    }
}

#endif
