#ifndef ACTOR_POOL_IMPL_HPP
#define ACTOR_POOL_IMPL_HPP

namespace HyperCave {
    inline void ActorPool::ResetGame()
    {
        m_actors_queued.clear();
        m_actors.clear();
        m_reset = false;
        Game::GetSingleton().LoadGame();
    }
}

#endif
