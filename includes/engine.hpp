#ifndef ENGINE_HPP
#define ENGINE_HPP

namespace HyperCave {
    enum ExitCode : int
    {
        ERROR = -1,
        SUCCESS = 0
    };

    namespace CONSTS {
        constexpr size_t GL_ERR_SIZE = 512;
    }

    namespace detail {
        template <typename T>
        class Singleton
        {
        private:
            inline static T* m_singleton = nullptr;
     
        protected:
            Singleton()
            {
                if (m_singleton) {
                    throw std::logic_error("Cannot have more than one instance of singleton class");
                } else {
                    m_singleton = static_cast<T*>(this);
                }
            }

            ~Singleton() noexcept
            {
                m_singleton = nullptr;
            }

        public:
            static T& GetSingleton() noexcept
            {
                assert(m_singleton);
                return static_cast<T&>(*m_singleton);
            }

            static T* GetSingletonPtr() noexcept
            {
                assert(m_singleton);
                return static_cast<T*>(m_singleton);
            }
        };

        class NoCopy
        {
        public:
            virtual ~NoCopy() = default;
        protected:
            NoCopy() = default;
        private:
            NoCopy(const NoCopy&) = delete;
            NoCopy(NoCopy&&) = delete;
            NoCopy& operator=(const NoCopy&) = delete;
        };
    }

    namespace ShaderSources {
        const char* sprite_vs = TOSTRING(
            \n#version 410\n

            layout (location = 0) in vec3 aPos;
            layout (location = 1) in vec2 aTexCoord;

            out vec2 uv;
            out vec4 color;

            uniform mat4 u_ViewProjection;
            uniform mat4 u_Model;
            uniform vec2 u_uv_offset;
            uniform vec4 u_Color;

            void main()
            {
                gl_Position = u_ViewProjection * u_Model * vec4(aPos, 1.0);
                uv = aTexCoord + u_uv_offset;
                color = u_Color;
            }
        );

        const char* sprite_fs = TOSTRING(
            \n#version 410\n

            \n#define MIN_ALPHA 0.1\n

            in vec2 uv;
            in vec4 color;

            uniform sampler2D sprite;

            out vec4 FragColor;

            void main()
            {
                vec4 tex_color = texture(sprite, uv);

                if (tex_color.a < MIN_ALPHA) {
                    discard;
                }

                FragColor =  tex_color * color;

                float average = 0.2126 * FragColor.x + 0.7152 * FragColor.y + 0.0722 * FragColor.z;
                FragColor = vec4(vec3(average), 1);
                
                if (FragColor.x <= 0.25)
                {
                    FragColor = vec4(0.06, 0.22, 0.06, 1);
                }
                else if (FragColor.x > 0.75)
                {
                    FragColor = vec4(0.6, 0.74, 0.06, 1);
                }
                else if (FragColor.x > 0.25 && FragColor.x <= 0.5)
                {
                    FragColor = vec4(0.19, 0.38, 0.19, 1);
                }
                else
                {
                    FragColor = vec4(0.54, 0.67, 0.06, 1);
                }   
            }
        );
    }

    class SoundEngineWrapper final : public detail::Singleton<SoundEngineWrapper>
    {
    private:
        irrklang::ISoundEngine* m_sound_engine;
    public:
        SoundEngineWrapper() : m_sound_engine{irrklang::createIrrKlangDevice()}
        {
            if (!m_sound_engine) {
                throw std::runtime_error("Unable to create sound engine");
            }
        }

        ~SoundEngineWrapper() noexcept
        {
            m_sound_engine->drop();
        }


        irrklang::ISoundEngine* operator->() noexcept
        {
            return m_sound_engine;
        }
    };

    class GameTime final : public detail::Singleton<GameTime>
    {
    private:
        const decltype(std::chrono::high_resolution_clock::now()) m_start_time;
    public:
        GameTime() noexcept : m_start_time{std::chrono::high_resolution_clock::now()}
        {

        };

        template <typename T>
        decltype(auto) GetTimeSinceStart() const noexcept
        {
            return std::chrono::duration_cast<T>(std::chrono::high_resolution_clock::now() - m_start_time).count();
        }
    };

    struct InputManager final
    {
        std::unordered_map<std::string, std::function<void(double xpos, double ypos)>> m_cursor_pos_callbacks;
        std::unordered_map<std::string, std::function<void(int button, int action, int mods)>> m_mouse_button_callbacks;
    };

    class Camera final
    {
    private:
        glm::vec3 m_position;
        glm::vec3 m_up;
        glm::vec3 m_forward;
        float m_zoom;
        float m_world_size;
        float m_near;
        float m_far;
        float m_window_ratio;
        glm::vec2 m_world_min;
        glm::vec2 m_world_max;
        glm::mat4 m_projection_matrix;
        glm::mat4 m_camera_matrix;
        glm::mat4 m_view_projection_matrix;
        glm::mat4 m_screen_to_camera;
        glm::mat4 m_screen_to_world;
    public:
        Camera(const glm::vec3& position, const glm::vec3& up, const glm::vec3& forward, float zoom, float world_size, float near, float far) noexcept : m_position{position}, 
                                                                                                                                                         m_up{up}, 
                                                                                                                                                         m_forward{forward}, 
                                                                                                                                                         m_zoom{zoom}, 
                                                                                                                                                         m_world_size{world_size}, 
                                                                                                                                                         m_near{near}, 
                                                                                                                                                         m_far{far}, 
                                                                                                                                                         m_window_ratio{1.0f}
        {
            UpdateCameraMatrix();
        }

        void SetPosition(const glm::vec2& position) noexcept
        {
            m_position.x = position.x;
            m_position.y = position.y;
            UpdateCameraMatrix();
            UpdateCaches();
        }

        void Translate(const glm::vec2& direction) noexcept
        {
            m_position.x += direction.x;
            m_position.y += direction.y;
            UpdateCameraMatrix();
            UpdateCaches();
        }

        void SetZoom(float zoom) noexcept
        {
            m_zoom = zoom;
            m_camera_matrix[0][0] *= m_zoom;
            m_camera_matrix[1][1] *= m_zoom;
            UpdateCaches();
        }

        void SetUp(const glm::vec3& up) noexcept
        {
            m_up = up;
            UpdateCameraMatrix();
            UpdateCaches();
        }

        void SetForward(const glm::vec3& forward) noexcept
        {
            m_forward = forward;
            UpdateCameraMatrix();
            UpdateCaches();
        }

        void SetWorldSize(float world_size) noexcept
        {
            m_world_size = world_size;
            UpdateWorldBounds();
            UpdateProjectionMatrix();
            UpdateCaches();
        }

        std::pair<glm::vec2, glm::vec2> GetWorldBounds() const noexcept
        {
            return std::make_pair(m_world_min, m_world_max);
        }

        void SetRatio(float ratio)
        {
            if (ratio <= 0) {
                throw std::runtime_error("Ratio must be positive");
            }

            m_window_ratio = ratio;
            UpdateWorldBounds();
            UpdateProjectionMatrix();
            UpdateCaches();
        }

        const glm::mat4& GetViewProjection() const noexcept
        {
            return m_view_projection_matrix;
        }

        const glm::mat4& GetScreenToWorldMatrix() const noexcept
        {
            return m_screen_to_world;
        }

    private:
        void UpdateCameraMatrix() noexcept
        {
            m_camera_matrix = glm::lookAt(m_position,
                                          m_position + m_forward, 
                                          m_up);
            m_camera_matrix[0][0] *= m_zoom;
            m_camera_matrix[1][1] *= m_zoom;
        }

        void UpdateProjectionMatrix() noexcept
        {
            m_projection_matrix = glm::ortho(-m_world_size * m_window_ratio, m_world_size * m_window_ratio,
                                             -m_world_size, m_world_size, m_near, m_far);
        }

        void UpdateCaches() noexcept
        {
            m_view_projection_matrix = m_projection_matrix * m_camera_matrix;
            m_screen_to_camera = glm::inverse(m_projection_matrix);
            m_screen_to_world = glm::inverse(m_view_projection_matrix);
        }

        void UpdateWorldBounds() noexcept
        {
            m_world_min = glm::vec2{-m_world_size * m_window_ratio, -m_world_size};
            m_world_max = glm::vec2{m_world_size * m_window_ratio, m_world_size};
        }
    };

    class Renderer final : public detail::Singleton<Renderer>
    {
    private:
        GLsizei m_width;
        GLsizei m_height;
        GLFWwindow* m_window;
        const InputManager& m_imanager;
        std::shared_ptr<Camera> m_camera;
    public: 
        Renderer(GLsizei width, GLsizei height, const InputManager& imanager, std::shared_ptr<Camera> camera) : m_width{width}, m_height{height}, m_window{nullptr}, m_imanager{imanager},
                                                                                                                m_camera{camera}
        {
            if (width <= 0 || height <= 0) {
                throw std::logic_error("Invalid window dimension");
            }

            if (!m_camera) {
                throw std::logic_error("Must attach camera to renderer");
            }

            glfwInit();
            glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
            glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
        #ifdef __APPLE__
            glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
        #endif
            glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
            glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_FALSE);
            glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

            m_window = glfwCreateWindow(m_width, m_height, "HyperCave", nullptr, nullptr);

            if (!m_window) {
                glfwTerminate();
                throw std::runtime_error("Failed to create a window");
            }

            // Create the viewport
            glViewport(0, 0, m_width, m_height);

            // Set the context
            glfwMakeContextCurrent(m_window);

            glfwSetWindowUserPointer(m_window, this);

            glfwSetFramebufferSizeCallback(m_window, [](GLFWwindow* window, GLsizei width, GLsizei height) {
                static_cast<Renderer*>(glfwGetWindowUserPointer(window))->UpdateViewport(width, height);
            });

            // Get the size of the screen
            glfwGetFramebufferSize(m_window, &m_width, &m_height);

            // setup the window ratio
            m_camera->SetRatio(static_cast<float>(m_width) / static_cast<float>(m_height));

            // Enable additional OpenGL features
            glewExperimental = GL_TRUE;

            glewInit();
            glGetError(); // Call it once to catch glewInit() bug
            
            // Enable GL error callback
            GLint flags;
            glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
            if (flags & GL_CONTEXT_FLAG_DEBUG_BIT) {
                glEnable(GL_DEBUG_OUTPUT);
                glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
                glDebugMessageCallback(GLDebugOutput, nullptr);
                glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
            }

            // register input callback functions
            glfwSetCursorPosCallback(m_window, [](GLFWwindow* window, double xpos, double ypos) {
                static_cast<Renderer*>(glfwGetWindowUserPointer(window))->CursorPosUpdate(xpos, ypos);
            });

            glfwSetMouseButtonCallback(m_window, [](GLFWwindow* window, int button, int action, int mods) {
                static_cast<Renderer*>(glfwGetWindowUserPointer(window))->MouseButtonUpdate(button, action, mods);
            });

            
            // hide mouse
            //glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
        }

        ~Renderer() noexcept
        {
            glfwTerminate();
        }

        void PollEvents() const noexcept
        {
            glfwPollEvents();
        }

        void SwapFrameBuffer() noexcept
        {
            glfwSwapBuffers(m_window);
        }

        bool ShouldClose() const noexcept
        {
            return glfwWindowShouldClose(m_window);
        }

        glm::vec2 GetWindowDimensions() const noexcept
        {
            return glm::vec2{m_width, m_height};
        }

        void SetCamera(std::shared_ptr<Camera> camera)
        {
            m_camera = camera;
            m_camera->SetRatio(static_cast<float>(m_width) / static_cast<float>(m_height));
        }

        const Camera& GetActiveCamera() const noexcept
        {
            return *m_camera;
        }

    private:
        static void APIENTRY GLDebugOutput(GLenum source,
            GLenum type,
            GLuint id,
            GLenum severity,
            GLsizei length,
            const GLchar *message,
            const void *userParam) noexcept
        {
            UNUSED(length);
            UNUSED(userParam);

            // ignore non-significant error/warning codes
            if (id == 131169 || id == 131185 || id == 131218 || id == 131204) return;

            std::cerr << "---------------" << '\n';
            std::cerr << "Debug message (" << id << "): " << message << '\n';

            switch (source)
            {
                case GL_DEBUG_SOURCE_API:             std::cerr << "Source: API"; break;
                case GL_DEBUG_SOURCE_WINDOW_SYSTEM:   std::cerr << "Source: Window System"; break;
                case GL_DEBUG_SOURCE_SHADER_COMPILER: std::cerr << "Source: Shader Compiler"; break;
                case GL_DEBUG_SOURCE_THIRD_PARTY:     std::cerr << "Source: Third Party"; break;
                case GL_DEBUG_SOURCE_APPLICATION:     std::cerr << "Source: Application"; break;
                case GL_DEBUG_SOURCE_OTHER:           std::cerr << "Source: Other"; break;
            } std::cerr << '\n';

            switch (type)
            {
                case GL_DEBUG_TYPE_ERROR:               std::cerr << "Type: Error"; break;
                case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: std::cerr << "Type: Deprecated Behaviour"; break;
                case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  std::cerr << "Type: Undefined Behaviour"; break;
                case GL_DEBUG_TYPE_PORTABILITY:         std::cerr << "Type: Portability"; break;
                case GL_DEBUG_TYPE_PERFORMANCE:         std::cerr << "Type: Performance"; break;
                case GL_DEBUG_TYPE_MARKER:              std::cerr << "Type: Marker"; break;
                case GL_DEBUG_TYPE_PUSH_GROUP:          std::cerr << "Type: Push Group"; break;
                case GL_DEBUG_TYPE_POP_GROUP:           std::cerr << "Type: Pop Group"; break;
                case GL_DEBUG_TYPE_OTHER:               std::cerr << "Type: Other"; break;
            } std::cerr << '\n';

            switch (severity)
            {
                case GL_DEBUG_SEVERITY_HIGH:         std::cerr << "Severity: high"; break;
                case GL_DEBUG_SEVERITY_MEDIUM:       std::cerr << "Severity: medium"; break;
                case GL_DEBUG_SEVERITY_LOW:          std::cerr << "Severity: low"; break;
                case GL_DEBUG_SEVERITY_NOTIFICATION: std::cerr << "Severity: notification"; break;
            } std::cerr << '\n';
            std::cerr << '\n';
        }

        void UpdateViewport(GLsizei width, GLsizei height) noexcept
        {
            glViewport(0, 0, width, height);

            m_width = width;
            m_height = height;

            m_camera->SetRatio(static_cast<float>(m_width) / static_cast<float>(m_height));
        }

        void CursorPosUpdate(double xpos, double ypos) const
        {
            for (auto&& i : m_imanager.m_cursor_pos_callbacks) {
                i.second(xpos, ypos);
            }
        }

        void MouseButtonUpdate(int button, int action, int mods) const
        {
            for (auto&& i : m_imanager.m_mouse_button_callbacks) {
                i.second(button, action, mods);
            }
        }
    };

    class Shader final : public detail::NoCopy
    {
    private:
        GLuint m_program;
    public:
        Shader(const char* vs, const char* fs)
        {
            if (!vs || !fs) {
                throw std::runtime_error("Vertex shader and fragment shader required");
            }

            GLuint vprog = glCreateShader(GL_VERTEX_SHADER);
            glShaderSource(vprog, 1, &vs, nullptr);
            glCompileShader(vprog);

            // check for shader compile errors
            GLint success;
            char infoLog[CONSTS::GL_ERR_SIZE];

            glGetShaderiv(vprog, GL_COMPILE_STATUS, &success);
            if (!success) {
                glGetShaderInfoLog(vprog, CONSTS::GL_ERR_SIZE, nullptr, infoLog);
                std::cerr << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << '\n';
            }

            GLuint fprog = glCreateShader(GL_FRAGMENT_SHADER);
            glShaderSource(fprog, 1, &fs, nullptr);
            glCompileShader(fprog);

            glGetShaderiv(fprog, GL_COMPILE_STATUS, &success);
            if (!success) {
                glGetShaderInfoLog(fprog, CONSTS::GL_ERR_SIZE, nullptr, infoLog);
                std::cerr << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << '\n';
            }

            // link shaders
            m_program = glCreateProgram();

            glAttachShader(m_program, vprog);
            glAttachShader(m_program, fprog);

            glLinkProgram(m_program);

            glDeleteShader(vprog);
            glDeleteShader(fprog);

            // check for linking errors
            glGetProgramiv(m_program, GL_LINK_STATUS, &success);
            if (!success) {
                glGetProgramInfoLog(m_program, CONSTS::GL_ERR_SIZE, nullptr, infoLog);
                std::cerr << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << '\n';   
                throw std::runtime_error("Failed to link shader");
            }
        }

        ~Shader() noexcept
        {
            glDeleteProgram(m_program);
        }

        void Bind() const noexcept
        {
            glUseProgram(m_program);
        }

        void SetFloat(const GLchar* name, GLfloat value, bool useShader = false) const noexcept
        {
            if (useShader) {
                Bind();
            }

            glUniform1f(glGetUniformLocation(m_program, name), value);
        }

        void SetInteger(const GLchar* name, GLint value, bool useShader = false) const noexcept
        {
            if (useShader) {
                Bind();
            }

            glUniform1i(glGetUniformLocation(m_program, name), value);
        }

        void SetVector2f(const GLchar* name, GLfloat x, GLfloat y, bool useShader = false) const noexcept
        {
            if (useShader) {
                Bind();
            }

            glUniform2f(glGetUniformLocation(m_program, name), x, y);
        }

        void SetVector2f(const GLchar* name, const glm::vec2& value, bool useShader = false) const noexcept
        {
            if (useShader) {
                Bind();
            }

            glUniform2f(glGetUniformLocation(m_program, name), value.x, value.y);
        }

        void SetVector3f(const GLchar* name, GLfloat x, GLfloat y, GLfloat z, bool useShader = false) const noexcept
        {
            if (useShader) {
                Bind();
            }

            glUniform3f(glGetUniformLocation(m_program, name), x, y, z);
        }

        void SetVector3f(const GLchar* name, const glm::vec3& value, bool useShader = false) const noexcept
        {
            if (useShader) {
                Bind();
            }

            glUniform3f(glGetUniformLocation(m_program, name), value.x, value.y, value.z);
        }

        void SetVector4f(const GLchar* name, GLfloat x, GLfloat y, GLfloat z, GLfloat w, bool useShader = false) const noexcept
        {
            if (useShader) {
                Bind();
            }

            glUniform4f(glGetUniformLocation(m_program, name), x, y, z, w);
        }

        void SetVector4f(const GLchar *name, const glm::vec4& value, bool useShader = false) const noexcept
        {
            if (useShader) {
                Bind();
            }

            glUniform4f(glGetUniformLocation(m_program, name), value.x, value.y, value.z, value.w);
        }

        void SetMatrix4(const GLchar *name, const glm::mat4& matrix, bool useShader = false) const noexcept
        {
            if (useShader) {
                Bind();
            }

            glUniformMatrix4fv(glGetUniformLocation(m_program, name), 1, GL_FALSE, glm::value_ptr(matrix));
        }
    };

    class Texture final : public detail::NoCopy
    {
    private:
        GLuint m_texture;
        float m_aspect;
    public:
        Texture(const char* source) : m_aspect{1.0f}
        {
            if (!source) {
                throw std::runtime_error("Texture source required");
            }

            int width, height, nrChannels;
            unsigned char* data = stbi_load(source, &width, &height, &nrChannels, 0);

            if (!data) {
                throw std::runtime_error("Failed to load texture");
            }

            GLenum format = GL_INVALID_ENUM;
            switch (nrChannels)
            {
                case 1:
                    format = GL_RED;
                    break;

                case 3:
                    format = GL_RGB;
                    break;

                case 4:
                    format = GL_RGBA;
                    break;

                default:
                    throw std::runtime_error("Invalid number of channels on texture");
            }

            m_aspect = static_cast<float>(width) / static_cast<float>(height);

            glGenTextures(1, &m_texture);
            glBindTexture(GL_TEXTURE_2D, m_texture);  

            // set texture options
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

            glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
            glGenerateMipmap(GL_TEXTURE_2D);

            stbi_image_free(data);
            glBindTexture(GL_TEXTURE_2D, 0);
        }

        void Bind() const noexcept
        {
            glBindTexture(GL_TEXTURE_2D, m_texture);
        }

        float GetAspect() const noexcept
        {
            return m_aspect;
        }

        ~Texture() noexcept
        {
            glDeleteTextures(1, &m_texture);
        }
    };

    class Mesh final : public detail::NoCopy
    {
    private:
        GLuint m_VAO;
        GLuint m_VBO;
        GLuint m_EBO;
        size_t m_indices_count;
    public:
        Mesh(const std::vector<GLfloat>& vertices, const std::vector<GLushort>& indices) : m_indices_count{indices.size()}
        {
            // generate the buffers
            glGenVertexArrays(1, &m_VAO);
            glGenBuffers(1, &m_VBO);
            glGenBuffers(1, &m_EBO);

            // bind the VAO
            glBindVertexArray(m_VAO);

            // bind the VBO
            glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
            glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(GLfloat), vertices.data(), GL_STATIC_DRAW);

            // bind the EBO
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBO);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLushort), indices.data(), GL_STATIC_DRAW);

            // vertex position attribute
            glEnableVertexAttribArray(0);
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), nullptr);

            // texture attribute
            glEnableVertexAttribArray(1);
            glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), reinterpret_cast<GLvoid*>(3 * sizeof(GLfloat)));

            // unbind the VBO
            glBindBuffer(GL_ARRAY_BUFFER, 0); 

            // unbind the VAO
            glBindVertexArray(0);

            // unbind the EBO
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        }

        ~Mesh() noexcept
        {
            glDeleteVertexArrays(1, &m_VAO);
            glDeleteBuffers(1, &m_VBO);
            glDeleteBuffers(1, &m_EBO);
        }

        void Draw() const noexcept
        {
            glBindVertexArray(m_VAO);
            glDrawElements(GL_TRIANGLES, m_indices_count, GL_UNSIGNED_SHORT, 0);
            glBindVertexArray(0); 
        }
    };

    class Transform final
    {
    private:
        glm::mat4 m_Model; // glm uses column-major matrices
    public:
        Transform() noexcept : m_Model{glm::mat4{1}}
        {

        }

        void Translate(const glm::vec2& direction) noexcept
        {
            m_Model[3][0] += direction.x;
            m_Model[3][1] += direction.y;
        }

        void SetPosition(const glm::vec2& new_position) noexcept
        {
            m_Model[3][0] = new_position.x;
            m_Model[3][1] = new_position.y;
        }

        void Scale(const glm::vec2& factor) noexcept
        {
            m_Model[0][0] *= factor.x;
            m_Model[1][1] *= factor.y;
        }

        glm::vec2 GetScale() const noexcept
        {
            return glm::vec2{m_Model[0][0], m_Model[1][1]};
        }

        glm::vec2 GetPosition() const noexcept
        {
            return m_Model[3];
        }

        void SetZPosition(float z) noexcept
        {
            m_Model[3][2] = z;
        }

        float GetZPosition() const noexcept
        {
            return m_Model[3][2];
        }

        const glm::mat4& GetMatrix() const noexcept
        {
            return m_Model;
        }
    };

    class CircleCollider final
    {
    private:
        glm::vec2 m_center;
        float m_radius;
    public:
        CircleCollider() noexcept : m_center{glm::vec2{0}}, m_radius{0.5f}
        {

        }

        bool IsColliding(const CircleCollider& other) noexcept
        {
            return (GetDistance(other.m_center) <= (m_radius + other.m_radius));
        }

        std::pair<bool, glm::vec2> IsInBounding(const glm::vec2& min, const glm::vec2& max) noexcept
        {
            if ((m_center.x - m_radius) < min.x) {
                return std::make_pair(false, glm::vec2{1, 0});
            }

            if ((m_center.x + m_radius) > max.x) {
                return std::make_pair(false, glm::vec2{-1, 0});
            }

            if ((m_center.y - m_radius) < min.y) {
                return std::make_pair(false, glm::vec2{0, 1});
            }

            if ((m_center.y + m_radius) > max.y) {
                return std::make_pair(false, glm::vec2{0, -1});
            }

            return std::make_pair(true, glm::vec2{0});
        }

        static glm::vec2 GetReflection(const glm::vec2& direction, const glm::vec2& normal) noexcept
        {
            return direction - 2.0f * (glm::dot(direction, normal)) * normal;
        }
        
        void SetRadius(float radius) noexcept
        {
            m_radius = radius;
        }

        float GetRadius() const noexcept
        {
            return m_radius;
        }

        void SetCenter(const glm::vec2& center) noexcept
        {
            m_center = center;
        }

        const glm::vec2& GetCenter() const noexcept
        {
            return m_center;
        }

        float GetDistance(const glm::vec2& point) const noexcept
        {
            return glm::distance(m_center, point);
        }
    };

    class Behavior
    {
    public:
        virtual void DoAction(class Actor& actor, float dt) = 0;

        virtual ~Behavior() = default;
    };

    struct Material : public detail::NoCopy
    {
        const Shader& m_shader;
        glm::vec4 m_color;
        Material(const Shader& shader) noexcept : m_shader{shader}, m_color{1.0f} { }
        virtual void Draw(const class Actor& data) const noexcept = 0;
        template <typename T>
        T& GetMaterial() noexcept
        {
            static_assert(std::is_base_of<Material, T>::value, "T must derive from material");
            return static_cast<T&>(*this);
        }
    };

    struct SpriteMaterial final : public Material
    {
        const Texture& m_texture;
        glm::vec2 m_uv_offset;

        SpriteMaterial(const Shader& shader, const Texture& texture) : Material{shader}, m_texture{texture}, m_uv_offset{glm::vec2{0}}
        {

        }

        inline void Draw(const class Actor& actor) const noexcept override;
    };

    class Actor final
    {
    public:
        std::string m_name;
        Transform m_transform;
        CircleCollider m_collider;
        std::unordered_map<std::type_index, std::unique_ptr<Behavior>> m_behaviors;
        std::unique_ptr<Material> m_material;
        bool m_is_active;
        bool m_destroy;

        Actor(std::string&& name) noexcept : m_name{std::move(name)}, m_is_active{true}, m_destroy{false}
        {

        }

        template <typename T, typename... Args>
        T& CreateBehavior(Args&&... args)
        {
            auto index = std::type_index(typeid(T));
            auto pair = m_behaviors.insert(std::make_pair(index,
                                        std::make_unique<T>(std::forward<Args>(args)...)));

            if (!pair.second) {
                throw std::runtime_error("Duplicate behavior");
            }

            return static_cast<T&>(*pair.first->second);
        }

        template <typename T>
        void RemoveComponent()
        {
            m_behaviors.erase(std::type_index(typeid(T)));
        }

        template <typename T>
        T* GetBehavior() noexcept
        {
            auto it = m_behaviors.find(std::type_index(typeid(T)));
            if (it == m_behaviors.end()) {
                return nullptr;
            } else {
                return it->second.get();
            }
        }

        void Tick(float dt)
        {
            for (auto&& i : m_behaviors) {
                i.second->DoAction(*this, dt);
            }
        }

        void Draw() const noexcept
        {
            m_material->Draw(*this);
        }
    };

    class ActorPool final
    {
    private:
        std::vector<std::shared_ptr<Actor>> m_actors;
        std::vector<std::shared_ptr<Actor>> m_actors_queued;
        bool m_reset;
    public:
        ActorPool() noexcept : m_reset{false}
        {

        }

        void AddActor(std::shared_ptr<Actor> actor)
        {
            if (m_reset) return; // don't add any more actors
            
            if (!actor) {
                throw std::runtime_error("Invalid actor");
            }

            m_actors_queued.emplace_back(actor);
        }

        void RemoveActor(const std::string& actor_name)
        {
            m_actors.erase(std::remove_if(m_actors.begin(), m_actors.end(), [&actor_name](const auto& i) {
                return i->m_name == actor_name;
            }), m_actors.end());
        }

        std::shared_ptr<Actor>& GetActor(const std::string& actor_name)
        {
            auto it = std::find_if(m_actors.begin(), m_actors.begin(), [&actor_name](const auto& i){
                return i->m_name == actor_name;
            });

            if (it == m_actors.end()) {
                throw std::runtime_error("Could not find actor");
            } else {
                return *it;
            }
        }

        void Tick(float dt, const Mesh& mesh)
        {
            // add new actors to the pool
            if (!m_actors_queued.empty()) {
                m_actors.insert(m_actors.end(), std::make_move_iterator(m_actors_queued.begin()), 
                            std::make_move_iterator(m_actors_queued.end()));
                m_actors_queued.clear();
            }
            
            // update all existing objects
            for (auto it = m_actors.begin(); it < m_actors.end();) {
                if ((*it)->m_destroy) {
                    it = m_actors.erase(it);
                } else {
                    if ((*it)->m_is_active) {
                        (*it)->Tick(dt);
                        (*it)->Draw();
                        mesh.Draw();
                    }
                    ++it;
                }
            }

            if (m_reset) {
                ResetGame();
            }
        }

        void Reset()
        {
            m_reset = true;
        }
    private:
        inline void ResetGame();
    };
}

#endif
