#ifndef GAME_HPP
#define GAME_HPP

namespace HyperCave {
    namespace GAMECONSTS {
        const glm::vec4 BGCOLOR = glm::vec4{0.06, 0.22, 0.06, 1};
        constexpr GLsizei WINDOW_WIDTH = 500;
        constexpr GLsizei WINDOW_HEIGHT = 300;
    }

    class Game final : public detail::Singleton<Game>
    {
        friend class ActorPool;
    public:
        InputManager m_imanager;
        std::shared_ptr<Camera> m_camera;
        std::unique_ptr<Renderer> m_renderer;
        ActorPool m_actor_pool;
        SoundEngineWrapper m_sound_engine;
        glm::vec2 m_mouse_pos;
        std::unordered_map<std::string, std::unique_ptr<Shader>> m_shaders;
        std::unordered_map<std::string, std::unique_ptr<Texture>> m_textures;
        std::unordered_map<std::string, std::string> m_audio;
        std::unique_ptr<Mesh> default_sprite_mesh;
        GameTime m_time;
    
        Game()
        {
            m_camera = std::make_shared<Camera>(glm::vec3{0}, 
                                                glm::vec3{0, 1, 0}, 
                                                glm::vec3{0, 0, -1}, 
                                                1.0f, 50.0f, -10.0f, 20.0f);

            m_renderer = std::make_unique<Renderer>(GAMECONSTS::WINDOW_WIDTH, GAMECONSTS::WINDOW_HEIGHT, m_imanager, m_camera); 

            default_sprite_mesh = std::make_unique<Mesh>
            (
                std::vector<GLfloat>
                {
                    0.5f, 0.5f, 0.0f, 1.0f, 1.0f,
                    0.5f, -0.5f, 0.0f, 1.0f, 0.0f,
                    -0.5f, -0.5f, 0.0f, 0.0f, 0.0f,
                    -0.5f, 0.5f, 0.0f, 0.0f, 1.0f
                },
                std::vector<GLushort>
                {
                    0, 2, 1,
                    0, 3, 2
                }
            );

            Config();
        }

        void Execute()
        {
            LoadGame();

            float delta_time = 0.0f;
            float last_frame = 0.0f;

            while (!m_renderer->ShouldClose()) {
                float current_frame = glfwGetTime();
                delta_time = current_frame - last_frame;
                last_frame = current_frame;

                m_renderer->PollEvents();
                glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
                m_actor_pool.Tick(delta_time, *default_sprite_mesh);

                m_renderer->SwapFrameBuffer();
            }
        }

        void RestartGame()
        {
            m_actor_pool.Reset();
        }

    private:
        void Config()
        {    
            ConfigureGraphics();
            ConfigureCallbacks();
            LoadAssets();
        }

        void ConfigureGraphics() noexcept
        {
            stbi_set_flip_vertically_on_load(true); //opengl uses different y-axis convention
            glEnable(GL_CULL_FACE);
            glCullFace(GL_BACK);
            glEnable(GL_DEPTH_TEST);
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            glClearColor(GAMECONSTS::BGCOLOR.x, 
                         GAMECONSTS::BGCOLOR.y, 
                         GAMECONSTS::BGCOLOR.z, 
                         GAMECONSTS::BGCOLOR.w);
        }

        void ConfigureCallbacks()
        {
            m_imanager.m_cursor_pos_callbacks["MousePos"] = [this](double xpos, double ypos) mutable {
                glm::vec2 window_size = this->m_renderer->GetWindowDimensions();
                glm::vec4 temp = glm::vec4((xpos * 4.0f / window_size.x) - 1, ((ypos * 4.0f / window_size.y) - 1) * -1, 0, 1);
                temp = this->m_camera->GetScreenToWorldMatrix() * temp;
                this->m_mouse_pos.x = temp.x;
                this->m_mouse_pos.y = temp.y;
            };
        }

        // game is simple so it's alright to cache everything in memory
        void LoadAssets()
        {
            LoadShaders();
            LoadTextures();
            LoadAudio();
        }

        void LoadShaders()
        {
            m_shaders["sprite_shader"] = std::make_unique<Shader>(ShaderSources::sprite_vs, ShaderSources::sprite_fs);
        }

        void LoadTextures()
        {
            m_textures["circle"] = std::make_unique<Texture>("resources/textures/circle.png");
        }

        void LoadAudio()
        {
            m_audio["bgm"] = std::move("resources/audio/bgmusic.wav");

            // preload all audio files
            for (auto&& i : m_audio) {
                if (!m_sound_engine->addSoundSourceFromFile(i.second.c_str(), irrklang::E_STREAM_MODE::ESM_AUTO_DETECT, true)) {
                    throw std::runtime_error("Error adding sound file");
                }
            }

            m_sound_engine->play2D(m_audio["bgm"].c_str(), true);
        }

        void LoadGame()
        {
            {
                std::shared_ptr<Actor> circle = std::make_shared<Actor>("Circle");
                circle->m_material = std::make_unique<SpriteMaterial>(*m_shaders["sprite_shader"], *m_textures["circle"]);
                circle->m_transform.Scale(glm::vec2{m_textures["circle"]->GetAspect() * 20.0f, 20.0f});
                circle->m_transform.SetZPosition(0.1f);
                circle->CreateBehavior<PlayerBehavior>();
                circle->m_collider.SetRadius(10.0f);
                m_actor_pool.AddActor(std::move(circle));
            }
        }
    };
}

#include "actorpoolimpl.hpp"
#include "materialimpl.hpp"
#include "behaviorsimpl.hpp"

#endif
