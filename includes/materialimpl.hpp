#ifndef MATERIALIML_HPP
#define MATERIALIML_HPP

namespace HyperCave {
    inline void SpriteMaterial::Draw(const Actor& actor) const noexcept
    {
        m_shader.Bind();
        m_shader.SetMatrix4("u_ViewProjection", Renderer::GetSingleton().GetActiveCamera().GetViewProjection());
        m_shader.SetVector2f("u_uv_offset", m_uv_offset);
        m_shader.SetVector4f("u_Color", m_color);
        m_shader.SetMatrix4("u_Model", actor.m_transform.GetMatrix());
        m_texture.Bind();
    }
}

#endif
