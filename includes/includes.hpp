#ifndef INCLUDES_HPP
#define INCLUDES_HPP

#define GLEW_STATIC
#include <GL/glew.h>
#ifdef __APPLE__
#include <OpenGL/gl.h>
#endif

#include <GLFW/glfw3.h>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include "IrrKlang/include/irrKlang.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <stdexcept>
#include <iostream>
#include <memory>
#include <string>
#include <typeinfo>
#include <typeindex>
#include <map>
#include <unordered_map>
#include <vector>
#include <functional>
#include <algorithm>
#include <chrono>
#include <random>

#endif
