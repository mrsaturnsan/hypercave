#ifndef BEHAVIORS_HPP
#define BEHAVIORS_HPP

namespace HyperCave {
    class Timer final : public Behavior
    {
    private:
        float m_duration;
        std::function<void(class Actor&)> m_callback;
    public:
        inline Timer(float duration, std::function<void(class Actor&)> callback) noexcept;
        inline virtual void DoAction(class Actor& actor, float dt) override;
    };

    class BulletBehavior final : public Behavior
    {
    private:
        glm::vec2 m_move_direction;
        float m_move_speed;
    public:
        inline BulletBehavior(const glm::vec2& move_direction, float move_speed) noexcept;
        inline virtual void DoAction(class Actor& actor, float dt) override;
    };

    class PlayerBehavior final : public Behavior
    {
    private:
        static constexpr float DRAG = 0.4f;
        static constexpr float BULLET_VELOCITY = 100.0f;
        static constexpr float KNOCK_BACK = 60.0f;

        glm::vec2 m_velocity;
        unsigned m_bullet_damage = 3;
        float m_bullet_speed = 3.0f;
        bool m_shoot;
    public:
        inline PlayerBehavior();
        inline ~PlayerBehavior() noexcept;
        inline virtual void DoAction(class Actor& actor, float dt) override;
    private:
        inline void Shoot(const glm::vec2& actorpos);
        inline void KnockBack(const glm::vec2& direction);
    };
}

#endif